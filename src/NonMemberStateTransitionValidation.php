<?php

namespace Drupal\og_nonmember_editor;

use Drupal\content_moderation\StateTransitionValidation;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\workflows\Transition;
use Drupal\og\Og;
use Drupal\og\Entity\OgRole;

/**
 * Custom State Transition Validation for group content.
 */
class NonMemberStateTransitionValidation extends StateTransitionValidation {

  /**
   * {@inheritdoc}
   */
  public function getValidTransitions(ContentEntityInterface $entity, AccountInterface $user) {
    // Get available workflow for entity.
    $workflow = $this->moderationInfo->getWorkflowForEntity($entity);

    // If no workflow is defined, just exit.
    if (empty($workflow)) {
      return;
    }

    // Get the current state of the entity.
    $current_state = $entity->moderation_state->value ? $workflow->getTypePlugin()->getState($entity->moderation_state->value) : $workflow->getTypePlugin()->getInitialState($entity);

    // Get group from entity.
    $group = $entity->og_audience->referencedEntities();
    $group = reset($group);

    // Provide default permission empty group.
    if (empty($group)) {
      return array_filter($current_state->getTransitions(), function (Transition $transition) use ($workflow, $user) {
        return $user->hasPermission('use ' . $workflow->id() . ' transition ' . $transition->id());
      });
    }

    // If a user is not a member,
    // check available permissions for non members.
    if (!Og::isMember($group, $user)) {
      $non_member_roles = OgRole::loadByGroupAndName($group, 'non-member');
      // Returns permission validation for group content non members.
      return array_filter($current_state->getTransitions(), function (Transition $transition) use ($workflow, $non_member_roles, $entity, $user) {
        // Check if the current user has global permission for moderation.
        // This is for administrators.
        $has_global_permission = $user->hasPermission($entity->bundle() . ' ' . $workflow->id() . ' transition ' . $transition->id());

        // Check for non member group content operation permission.
        $has_group_permission = $non_member_roles->hasPermission($entity->bundle() . ' ' . $workflow->id() . ' transition ' . $transition->id());
        return $has_global_permission || $has_group_permission;
      });
    }

    // Fetch user membership.
    $membership = Og::getMembership($group, $user);

    // Returns permission validation for group content members.
    return array_filter($current_state->getTransitions(), function (Transition $transition) use ($workflow, $membership, $entity) {
      return $membership->hasPermission($entity->bundle() . ' ' . $workflow->id() . ' transition ' . $transition->id());
    });
  }

}
