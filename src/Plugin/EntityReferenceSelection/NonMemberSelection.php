<?php

namespace Drupal\og_nonmember_editor\Plugin\EntityReferenceSelection;

use Drupal\og\Plugin\EntityReferenceSelection\OgSelection;
use Drupal\og\Og;

/**
 * Entity Reference Selection with distinguishing field.
 *
 * @EntityReferenceSelection(
 *   id = "non_member",
 *   label = @Translation("Group: Non-member"),
 *   group = "non_member",
 * )
 */
class NonMemberSelection extends OgSelection {

  /**
   * Overrides ::buildEntityQuery.
   *
   * Return only group in the matching results.
   *
   * @param string|null $match
   *   (Optional) Text to match the label against. Defaults to NULL.
   * @param string $match_operator
   *   (Optional) The operation the matching should be done with. Defaults
   *   to "CONTAINS".
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The EntityQuery object with the basic conditions and sorting applied to
   *   it.
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    // Getting the original entity selection handler. OG selection handler using
    // the default selection handler of the entity, which the field reference
    // to, and add another logic to the query object i.e. check if the entities
    // bundle defined as group.
    $query = $this->getSelectionHandler()->buildEntityQuery($match, $match_operator);
    $target_type = $this->configuration['target_type'];
    $definition = \Drupal::entityTypeManager()->getDefinition($target_type);

    if ($bundle_key = $definition->getKey('bundle')) {
      $bundles = Og::groupTypeManager()->getGroupBundlesByEntityType($target_type);

      if (!$bundles) {
        // If there are no bundles defined, we can return early.
        return $query;
      }
      $query->condition($bundle_key, $bundles, 'IN');
    }

    // Get the identifier key of the entity.
    $identifier_key = $definition->getKey('id');

    if ($this->currentUser->isAnonymous()) {
      // @todo: Check if anonymous users should have access to any referenced
      // groups? What about groups that allow anonymous posts?
      return $query->condition($identifier_key, -1);
    }

    return $query;
  }

}
