<?php

namespace Drupal\og_nonmember_editor\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Checks if referenced entities are valid.
 */
class GroupContentModerationConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {
    $groups = $entity->referencedEntities();
    $group = reset($groups);

    // Do nothing if no group is chosen.
    if (empty($groups)) {
      return;
    }

    // Get the base entity.
    $root_entity = $entity->getEntity();

    // Get the current user.
    $user = \Drupal::currentUser()->getAccount();

    // @var \Drupal\og_nonmember_editor\NonMemberStateTransitionValidation
    $transition_validation = \Drupal::service('og_nonmember_editor.state_transition_validation');

    // Get the transitions a user is allowed to do.
    $transitions = $transition_validation->getValidTransitions($root_entity, $user);

    // If no transitions are defined, bail.
    if (empty($transitions)) {
      return;
    }

    // Create access array base on transitions.
    $access = [];
    foreach ($transitions as $transition) {
      $access[] = $transition->to()->id();
    }

    // Check if the user is allowed to make transition.
    $is_allowed = in_array($root_entity->moderation_state->value, $access);

    // Throw an error when a user does not have permission to access content.
    if (!$is_allowed) {
      $this->context->addViolation($constraint->noPermissionToCreateTransition, ["%group" => $group->label()]);
    }
  }

}
