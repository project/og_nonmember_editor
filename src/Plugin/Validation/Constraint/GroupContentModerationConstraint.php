<?php

namespace Drupal\og_nonmember_editor\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Entity Reference valid reference constraint.
 *
 * Verifies that referenced entities are valid.
 *
 * @Constraint(
 *   id = "GroupContentModeration",
 *   label = @Translation("Custom Organic Groups valid reference", context = "Validation")
 * )
 */
class GroupContentModerationConstraint extends Constraint {

  /**
   * Not a valid group message.
   *
   * @var string
   */
  public $noPermissionToCreateTransition = "You are not allowed to publish content in %group";

  /**
   * Not a valid group message.
   *
   * @var string
   */
  public $notValidGroup = 'The entity %label is not defined as a group.';

  /**
   * Not a valid group message.
   *
   * @var string
   */
  public $notAllowedToPostInGroup = 'You are not allowed to post content in the group %label';

}
