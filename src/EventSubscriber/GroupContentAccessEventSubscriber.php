<?php

namespace Drupal\og_nonmember_editor\EventSubscriber;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\og\Event\PermissionEventInterface;
use Drupal\og\GroupContentOperationPermission;
use Drupal\og\PermissionManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\workflows\Entity\Workflow;

/**
 * Event subscribers for Organic Groups.
 */
class GroupContentAccessEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The OG permission manager.
   *
   * @var \Drupal\og\PermissionManagerInterface
   */
  protected $permissionManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The service providing information about bundles.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Constructs an OgEventSubscriber object.
   *
   * @param \Drupal\og\PermissionManagerInterface $permission_manager
   *   The OG permission manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The service providing information about bundles.
   */
  public function __construct(PermissionManagerInterface $permission_manager, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->permissionManager = $permission_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      PermissionEventInterface::EVENT_NAME => [
        ['provideDefaultNodePermissions', -1],
      ],
    ];
  }

  /**
   * Provides default permissions for the Node entity.
   *
   * @param \Drupal\og\Event\PermissionEventInterface $event
   *   The OG permission event.
   */
  public function provideDefaultNodePermissions(PermissionEventInterface $event) {
    $bundle_ids = $event->getGroupContentBundleIds();
    if (!array_key_exists('node', $bundle_ids)) {
      return;
    }
    $permissions = [];
    $bundle_info = $this->entityTypeBundleInfo->getBundleInfo('node');
    foreach ($bundle_ids['node'] as $bundle_id) {
      /** @var \Drupal\workflows\WorkflowInterface $workflow */
      foreach (Workflow::loadMultipleByType('content_moderation') as $id => $workflow) {
        foreach ($workflow->getTypePlugin()->getTransitions() as $transition) {
          $permission_values = [
            'name' => $bundle_id . ' ' . $workflow->id() . ' transition ' . $transition->id(),
            'title' => $this->t('%bundle %workflow workflow: %transition transition.', [
              '%workflow' => $workflow->label(),
              '%transition' => $transition->label(),
              '%bundle' => $bundle_info[$bundle_id]['label'],
            ]),
            'operation' => $transition->id(),
            'entity type' => 'node',
            'bundle' => $bundle_id,
          ];
          $permissions[] = new GroupContentOperationPermission($permission_values);
        }
      }
    }
    $event->setPermissions($permissions);
  }

}
