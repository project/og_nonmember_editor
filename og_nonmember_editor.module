<?php

/**
 * @file
 * Contains og_nonmember_editor.module.
 */

use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Implements hook_form_alter().
 */
function og_nonmember_editor_form_alter(&$form, &$form_state, $form_id) {
  // Alter GROUP CONTENT EDIT FORMS whose entity can be moderated.
  // Checks if current node is a group content and can be moderated.
  if (!isset($form['og_audience']) || !isset($form['moderation_state']) || $form_state->getFormObject()->getEntity()->isNew()) {
    return;
  }

  // @var \Drupal\Core\Session\AccountInterface.
  $account = \Drupal::currentUser();

  // @var \Drupal\og_nonmember_editor\NonMemberStateTransitionValidation
  $transition_validation = \Drupal::service('og_nonmember_editor.state_transition_validation');

  // @var \Drupal\Core\Entity\EntityInterface
  $entity = $form_state->getFormObject()->getEntity();

  // Get the current state of the node.
  $current_state = $entity->moderation_state->value;

  // Get the transitions a user is allowed to do.
  $transitions = $transition_validation->getValidTransitions($entity, $account);

  // New value for form states.
  $target_states = [];

  // Set the value for the new target states.
  foreach ($transitions as $transition) {
    $target_states[$transition->to()->id()] = $transition->to()->label();
  }

  // Alter the moderation state field widget to new value.
  $form['moderation_state']['widget'][0]['state'] = [
    '#type' => 'select',
    '#title' => t('Change to'),
    '#options' => $target_states,
    '#key_column' => 'value',
  ];

  $form['#theme'] = ['entity_moderation_form'];
  $form['#attached']['library'][] = 'content_moderation/content_moderation';
}

/**
 * Implements hook_entity_bundle_field_info_alter().
 */
function og_nonmember_editor_entity_bundle_field_info_alter(&$fields, EntityTypeInterface $entity_type, $bundle) {
  if ($entity_type->id() == 'node' && isset($fields['og_audience'])) {
    // Old validation still exist to this constraint.
    // Added new constraint for content moderation.
    $fields['og_audience']->addConstraint("GroupContentModeration");
  }
}
