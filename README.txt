OG Non-Member Editor
--------------------
This module allows anyone who has permission to create or edit content on a site
to create new content as unpublished drafts or new edits of existing content as
an unpubished draft, and assign them to groups they are not members of; members
of the groups may then publish the content.


Requirements
--------------------------------------------------------------------------------
The following are required:

* Content Moderation
  Included in Drupal core.
* OG
  https://www.drupal.org/project/og

Additionally, these patches are recommended if not required:
* Remove complex widget
  https://github.com/amitaibu/og/pull/317
* UI for Group roles and roles permissions
  https://github.com/Gizra/og/issues/238


Credits / contact
--------------------------------------------------------------------------------
Written by Wilmark Ganibo [1], comaintained by Damien McKenna [2].

Development is sponsored by Mediacurrent [3].

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://www.drupal.org/project/og_nonmember_editor


References
--------------------------------------------------------------------------------
1: https://www.drupal.org/u/macganibo
2: https://www.drupal.org/u/damienmckenna
3: https://www.mediacurrent.com/
